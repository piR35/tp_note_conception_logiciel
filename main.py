from random import random
import uvicorn
from fastapi import FastAPI, status
from randomsong import chanson_aleatoire

app = FastAPI()


@app.get("/", status_code = status.HTTP_200_OK)
def healthcheck():
    return {"healthcheck": "Tout est en ordre !"}

@app.get("/random/{artist_name}")
def findsong(artist_name):
    dico = chanson_aleatoire(artist_name)
    return dico

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=666)
