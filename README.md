# CONCEPTION DE LOGICIEL - TP NOTÉ

## Introduction

Nous avons implémenté une API musicale permettant, à partir du nom d'un artiste en entrée, de récupérer un titre aléatoire ainsi qu'un morceau des paroles de cette chanson.

Un scénario client est également disponible dans lequel, à partir d'un fichier json contenant les artistes préférés de Rudy, il est possible de créer une playlist de 20 titres.


## Quick start

```
git clone https://gitlab.com/piR35/tp_note_conception_logiciel.git
pip install -r requirements.txt
```

## Architecture de l'application

```mermaid
graph TD;
  randomsong.py-->main.py;
  main.py-->client.py;
  main.py-->test_api.py;
```

## Fonctionnalités

Pour démarrer l'API
```
python main.py
```
Puis rendez-vous sur http://localhost:666 : 

1) http://localhost:666/ pour tester l'état de santé de l'API

> Renvoie {"healthcheck": "Tout est en ordre !"} si elle fonctionne correctement

2) http://localhost:666/random/{nom_artiste} pour accéder à un titre au hasard de cet artiste


Pour créer une playlist à Rudy
```
python client.py
```

Pour lancer les tests unitaires
```
python test_api.py
```
