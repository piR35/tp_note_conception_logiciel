import requests
from random import randint

audiodb_artiste = "https://www.theaudiodb.com/api/v1/json/2/search.php"
audiodb_album = "https://www.theaudiodb.com/api/v1/json/2/album.php"
audiodb_titre = "https://www.theaudiodb.com/api/v1/json/2/track.php"
lyricsovh = "https://api.lyrics.ovh/v1/"


def chanson_aleatoire(nomartiste):

    param_nomartiste = {"s" : nomartiste}
    res_nomartiste = requests.get(url = audiodb_artiste, params = param_nomartiste)
    idartiste = res_nomartiste.json()["artists"][0]["idArtist"]


    param_idartiste = {"i" : idartiste}
    res_idartiste = requests.get(url = audiodb_album, params = param_idartiste)

    x = randint(0, len(res_idartiste.json()["album"]) - 1)
    idalbum = res_idartiste.json()["album"][x]["idAlbum"]


    param_idalbum = {"m" : idalbum}
    res_idalbum = requests.get(url = audiodb_titre, params = param_idalbum)

    y = randint(0, len(res_idalbum.json()["track"]) - 1)
    strtitre = res_idalbum.json()["track"][y]["strTrack"]


    res_titre = requests.get(url = lyricsovh + "/" + nomartiste + "/" + strtitre)
    if "lyrics" in res_titre.json():
        paroles = res_titre.json()["lyrics"].split("\r\n")[0]
    else:
        paroles = "Les paroles ne sont pas disponibles"

    
    dico = {
        "artist": nomartiste,
        "title": strtitre,
        "lyrics": paroles
    }
    return dico
