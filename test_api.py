from fastapi.testclient import TestClient
from main import app

def test_api():
    with TestClient(app) as client:

        # Test read_root
        response = client.get('/')
        response_json = response.json()
        assert isinstance(response_json, dict)

        # Test process
        response = client.get("/random/Rick Astley")
        response_json = response.json()
        assert isinstance(response_json, dict)
        assert response_json.get("artist") == "Rick Astley"

test_api()
print("Everything passed")
