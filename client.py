import json
from random import randint
from main import findsong

with open("rudy.json") as f:
    rudy = json.load(f)

n = len(rudy)
def makeplaylist():
    playlist = []
    while len(playlist) < 20:
        z = randint(0, n - 1)
        random_artist = rudy[z]["artiste"]
        random_song = findsong(random_artist)
        if not(random_song in playlist):
            playlist.append(random_song)
    return playlist

nouvelle_playlist = makeplaylist()
print(nouvelle_playlist)